﻿namespace PublicNote.Model.Datespans
{
  public class NoteDateSpanDTO
  {
    public string Name { get; set; }
    public int Value { get; set; }
  }

  public static partial class Extensions
  {
    public static NoteDateSpan ToNoteDateSpan(this NoteDateSpanDTO self)
    {
      return new NoteDateSpan
      {
        Name = self.Name,
        Value = self.Value
      };
    }
  }
}
