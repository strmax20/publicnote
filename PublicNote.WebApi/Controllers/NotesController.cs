﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using PublicNote.Model.Notes;

namespace PublicNote.WebApi.Controllers
{
  [Route("api/[controller]")]
  [EnableCors("CorsPolicy")]
  public class NotesController : Controller
  {
    private INoteService noteService;

    public NotesController(INoteService noteService)
    {
      this.noteService = noteService;
    }

    // GET api/notes
    [HttpGet]
    [ExceptionInfoFilter]
    public async Task<IEnumerable<NoteDTO>> Get()
    {
      return await Task.Run(() =>
      {
        return this.noteService.GetDtoList();
      });
    }

    // GET api/notes/5
    [HttpGet("{id}")]
    [ExceptionInfoFilter]
    public async Task<NoteDTO> Get(int id)
    {
      //NoteDTO
      return await Task.Run(() =>
      {
        return this.noteService.GetDTO(id);
      });
    }

    // POST api/notes
    [HttpPost]
    [ExceptionInfoFilter]
    public async Task<IActionResult> Post([FromBody]NoteDTO item)
    {
      return await Task.Run(() =>
      {
        return Ok(this.noteService.Add(item));
      });
    }

    // PUT api/notes/5
    [HttpPut("{id}")]
    [ExceptionInfoFilter]
    public async Task<IActionResult> Put(int id, [FromBody]NoteDTO item)
    {
      return await Task.Run(() =>
      {
        return Ok(this.noteService.Update(item));
      });
    }
  }
}
