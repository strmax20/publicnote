import {Component} from '@angular/core';
import {NotesService} from '../Services/notes.service';
import {INote, Note} from '../Contracts/INote';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent{

  private notes: INote[];
  private notesService: NotesService;
  private showList: boolean;

  constructor(
    private notesSrv: NotesService,
    private toastr: ToastrService)
  {
    this.notesService = notesSrv;

      // get notes list
      this.notesService.GetNoteList().subscribe(
        data =>
        {
          this.showList = true;
          this.notes = data as INote[];
        },
        error =>
        {
          this.showList = false;
          this.toastr.error('Can`t retrieve notes list', 'Server error');
        }
      );
  }
}
