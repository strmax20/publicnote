import {Inject} from "@angular/core";

export class ConfigService{

  private editorConfig: IEditorConfig;
  private environmentConfig: IEnvironmentConfig;

  constructor(@Inject('editorConfig') private editorCfg: IEditorConfig, @Inject('environmentConfig') private environmentCfg: IEnvironmentConfig)
  {
    this.editorConfig = editorCfg;
    this.environmentConfig = environmentCfg;
  }

  get EditorConfig()
  {
    return this.editorConfig;
  }

  get EnvironmentConfig()
  {
    return this.environmentConfig;
  }
}
