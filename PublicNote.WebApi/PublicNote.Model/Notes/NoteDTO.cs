﻿using System;

namespace PublicNote.Model.Notes
{
  public class NoteDTO
  {
    public DateTime ActualDate { get; set; }
    public int Id { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
    public DateTime CreateDate { get; set; }
    public bool NeverExpired { get; set; }
    public int SelectedDatespan { get; set; }
  }

  public static partial class Extensions
  {
    public static Note ToNote(this NoteDTO self)
    {
      return new Note
      {
        Body = self.Body,
        SelectedDatespan = self.SelectedDatespan,
        Title = self.Title,
        Id = self.Id
      };
    }
  }
}
