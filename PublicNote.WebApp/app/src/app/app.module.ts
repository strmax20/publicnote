import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatSelectModule, MatInputModule,
  MatToolbarModule, MatCardModule, MatListModule, MatMenuModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxEditorModule } from 'ngx-editor';
import { NoteEditComponent} from './NodeEdit/note-edit.component';
import { ConfigService } from './Services/config.service';
import { RouterModule, Routes } from '@angular/router';
import { NoteListComponent } from './NoteList/note-list.component';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { NoteViewComponent } from './NoteView/note-view.component';
import {NotesService} from './Services/notes.service';
import {DatespansService} from './Services/datespans.service';
import {ToastrModule} from 'ngx-toastr';

const appRoutes: Routes = [
  {
    path: 'edit/:id',
    component: NoteEditComponent
  },
  {
    path: 'view/:id',
    component: NoteViewComponent
  },
  {
    path: 'new',
    component: NoteEditComponent
  },
  {
    path: '',
    component: NoteListComponent,
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NoteEditComponent,
    NoteListComponent,
    NoteViewComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    NgxEditorModule,
    MatCardModule,
    MatListModule,
    MatToolbarModule,
    MatMenuModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    ConfigService,
    [
      {
        provide: 'editorConfig', useFactory: function () {
        return {
          'editable': true,
          'spellcheck': true,
          'height': '5rem',
          'minHeight': '2rem',
          'placeholder': 'Enter text here...',
          'translate': 'no',
          'toolbar': []
        }; }
      },
      {
        provide: 'environmentConfig', useFactory: function () {
        return {
          'apiBaseUrl': 'http://localhost:3000/api'
        }; }
      }
    ],
    NotesService,
    DatespansService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
