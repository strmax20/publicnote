﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace PublicNote.WebApi
{
  public class ExceptionInfoFilter : ExceptionFilterAttribute
  {
    public override void OnException(ExceptionContext context)
    {
      if (context.Exception != null)
        context.Result = new ContentResult() { Content = context.Exception.Message, StatusCode = 400 };
    }
  }
}
