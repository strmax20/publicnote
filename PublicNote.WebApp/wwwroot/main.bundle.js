webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/Contracts/INote.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Note; });
var Note = (function () {
    function Note() {
        this.id = 0;
        this.title = '';
        this.body = '';
        this.createDate = new Date();
        this.actualDate = new Date();
        this.selectedDatespan = 0;
        this.neverExpired = true;
    }
    return Note;
}());



/***/ }),

/***/ "../../../../../src/app/NodeEdit/note-edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".button {\r\n  margin: 8px 0;\r\n}\r\n\r\n.card-wrapper\r\n{\r\n  max-width: 800px;\r\n  padding: 20px;\r\n}\r\n\r\n.example-wrapper {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  max-width: 800px;\r\n}\r\n\r\n.form-wrapper > * {\r\n  width: 100%;\r\n}\r\n.label {\r\n  color: rgba(0,0,0,.54);\r\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\r\n  font-weight: 400;\r\n  padding: 8px 0;\r\n}\r\n\r\n.page-wrapper {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  -webkit-box-align: center;\r\n      -ms-flex-align: center;\r\n          align-items: center;\r\n}\r\n\r\n.select-wrapper{\r\n   padding-top: 12px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/NodeEdit/note-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-wrapper\">\n  <mat-card class=\"card-wrapper\">\n\n    <mat-toolbar>Edit note {{note.title}}</mat-toolbar>\n    <div class=\"form-wrapper\">\n\n      <mat-form-field>\n        <input matInput type=\"text\" [(ngModel)]=\"note.title\" placeholder=\"Name\">\n      </mat-form-field>\n\n      <div class=\"label\">Content</div>\n      <app-ngx-editor [config]=\"editorConfig\" [(ngModel)]=\"htmlContent\"></app-ngx-editor>\n\n      <mat-form-field class=\"select-wrapper\">\n        <mat-select [(ngModel)]=\"note.selectedDatespan\" [placeholder]=\"'Expired in'\">\n          <mat-option *ngFor=\"let datespan of datespans\" [value]=\"datespan.value\">\n            {{ datespan.name }}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <button mat-button class=\"button\" (click)=\"Save($event)\">\n      Save\n    </button>\n    <button mat-button class=\"button\" (click)=\"Back()\">\n      Back\n    </button>\n  </mat-card>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/NodeEdit/note-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoteEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_config_service__ = __webpack_require__("../../../../../src/app/Services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_notes_service__ = __webpack_require__("../../../../../src/app/Services/notes.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Contracts_INote__ = __webpack_require__("../../../../../src/app/Contracts/INote.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Services_datespans_service__ = __webpack_require__("../../../../../src/app/Services/datespans.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/toastr.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NoteEditComponent = (function () {
    function NoteEditComponent(config, notesService, location, route, datespansService, router, toastr) {
        var _this = this;
        this.config = config;
        this.notesService = notesService;
        this.location = location;
        this.route = route;
        this.datespansService = datespansService;
        this.router = router;
        this.toastr = toastr;
        this.id = this.route.snapshot.params.id;
        this.note = new __WEBPACK_IMPORTED_MODULE_5__Contracts_INote__["a" /* Note */]();
        // get datespans
        datespansService.GetDatespanList().subscribe(function (data) {
            _this.datespans = data;
        });
        // get data from server
        if (this.id != null) {
            this.notesService.GetNote(this.id).subscribe(function (data) {
                if (!data) {
                    _this.router.navigate(['/']);
                }
                _this.note = data;
                _this.htmlContent = _this.note.body;
            });
        }
        // configure default config to edit mode
        this.editorConfig = this.config.EditorConfig;
        this.editorConfig.editable = true;
        // apply default toolbars set
        this.editorConfig.toolbar = [];
    }
    NoteEditComponent.prototype.Back = function () {
        return this.location.back();
    };
    NoteEditComponent.prototype.Save = function () {
        var _this = this;
        this.note.body = this.htmlContent;
        (this.id != null) ?
            this.notesService.UpdateNote(this.note).subscribe(function (info) {
                console.log(info);
                _this.toastr.success('Note '.concat(info.title).concat(' updated'), 'Success');
            }) :
            this.notesService.CreateNote(this.note).subscribe(function (info) {
                console.log(info);
                _this.toastr.success('Note '.concat(info.title).concat(' created'), 'Success');
            });
    };
    NoteEditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/NodeEdit/note-edit.component.html"),
            selector: 'app-root',
            styles: [__webpack_require__("../../../../../src/app/NodeEdit/note-edit.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__Services_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_4__Services_notes_service__["a" /* NotesService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_6__Services_datespans_service__["a" /* DatespansService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__["b" /* ToastrService */]])
    ], NoteEditComponent);
    return NoteEditComponent;
}());



/***/ }),

/***/ "../../../../../src/app/NoteList/note-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".button {\r\n  margin: 8px 0;\r\n}\r\n\r\n.card-wrapper\r\n{\r\n  max-width: 800px;\r\n  padding: 20px;\r\n}\r\n\r\n.form-wrapper {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  max-width: 800px;\r\n  min-width: 600px;\r\n}\r\n\r\n.form-wrapper > * {\r\n  width: 100%;\r\n}\r\n\r\n.header-wrapper {\r\n  padding: 30px 30px 0 30px;\r\n  font-weight: 600;\r\n}\r\n\r\n.label {\r\n  color: rgba(0,0,0,.54);\r\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\r\n  font-weight: 400;\r\n  padding: 8px 0;\r\n}\r\n\r\n.list-item {\r\n  text-align: left;\r\n}\r\n\r\n.page-wrapper {\r\n  display: -webkit-box;\r\n  display: -ms-flexbox;\r\n  display: flex;\r\n  -webkit-box-orient: vertical;\r\n  -webkit-box-direction: normal;\r\n      -ms-flex-direction: column;\r\n          flex-direction: column;\r\n  -webkit-box-align: center;\r\n      -ms-flex-align: center;\r\n          align-items: center;\r\n}\r\n\r\n.select-wrapper{\r\n   padding-top: 12px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/NoteList/note-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-wrapper\">\n  <mat-card class=\"card-wrapper\">\n    <mat-toolbar>Notes</mat-toolbar>\n    <mat-card *ngIf=\"!showList\">Nothing there</mat-card>\n    <button *ngIf=\"showList\" mat-button class=\"main-button\" routerLink='/new'>\n      Create new note\n    </button>\n\n    <div *ngIf=\"showList\" class=\"form-wrapper\">\n\n      <div class=\"header-wrapper\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\n        <span class=\"header-item\" fxFlex=\"70%\">Title</span>\n        <span class=\"header-item\" fxFlex=\"30%\">Expired in</span>\n      </div>\n\n      <mat-list *ngFor=\"let note of notes\" fxLayout=\"row\" fxLayoutAlign=\"start start\">\n        <button mat-button routerLink='/view/{{note.id}}' fxFlex=\"100%\">\n          <mat-list-item fxLayout=\"row\"  fxLayoutAlign=\"start start\" >\n            <span class=\"list-item\" fxFlex=\"70%\">{{note.title}}</span>\n            <span class=\"list-item\" *ngIf=\"note.neverExpired === false\" fxFlex=\"30%\">{{note.actualDate | date:\"dd/MM/yyyy HH:mm\"}}</span>\n            <span class=\"list-item\" *ngIf=\"note.neverExpired\" fxFlex=\"30%\">Never expired</span>\n          </mat-list-item>\n        </button>\n      </mat-list>\n\n    </div>\n  </mat-card>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/NoteList/note-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoteListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Services_notes_service__ = __webpack_require__("../../../../../src/app/Services/notes.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/toastr.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NoteListComponent = (function () {
    function NoteListComponent(notesSrv, toastr) {
        var _this = this;
        this.notesSrv = notesSrv;
        this.toastr = toastr;
        this.notesService = notesSrv;
        // get notes list
        this.notesService.GetNoteList().subscribe(function (data) {
            _this.showList = true;
            _this.notes = data;
        }, function (error) {
            _this.showList = false;
            _this.toastr.error('Can`t retrieve notes list', 'Server error');
        });
    }
    NoteListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/NoteList/note-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/NoteList/note-list.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__Services_notes_service__["a" /* NotesService */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_toastr__["b" /* ToastrService */]])
    ], NoteListComponent);
    return NoteListComponent;
}());



/***/ }),

/***/ "../../../../../src/app/NoteView/note-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*.button {*/\r\n  /*margin: 8px 0;*/\r\n/*}*/\r\n\r\n/*.card-wrapper*/\r\n/*{*/\r\n  /*max-width: 800px;*/\r\n  /*padding: 20px;*/\r\n/*}*/\r\n\r\n/*.example-wrapper {*/\r\n  /*display: flex;*/\r\n  /*flex-direction: column;*/\r\n  /*max-width: 800px;*/\r\n  /*min-width: 600px;*/\r\n/*}*/\r\n\r\n/*.form-wrapper > * {*/\r\n  /*width: 100%;*/\r\n/*}*/\r\n\r\n/*.header-wrapper {*/\r\n  /*padding: 30px 30px 0 30px;*/\r\n  /*font-weight: 600;*/\r\n/*}*/\r\n\r\n/*.label {*/\r\n  /*color: rgba(0,0,0,.54);*/\r\n  /*font-family: Roboto, \"Helvetica Neue\", sans-serif;*/\r\n  /*font-weight: 400;*/\r\n  /*padding: 8px 0;*/\r\n/*}*/\r\n\r\n/*.list-item {*/\r\n  /*text-align: left;*/\r\n/*}*/\r\n\r\n/*.page-wrapper {*/\r\n  /*display: flex;*/\r\n  /*flex-direction: column;*/\r\n  /*align-items: center;*/\r\n/*}*/\r\n\r\n/*.select-wrapper{*/\r\n   /*padding-top: 12px;*/\r\n/*}*/\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/NoteView/note-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-wrapper\">\n  <mat-card class=\"card-wrapper\">\n\n    <mat-toolbar>View note {{note.title}}</mat-toolbar>\n    <div class=\"form-wrapper\">\n\n      <mat-form-field>\n        <input matInput type=\"text\" disabled=\"disabled\" [(ngModel)]=\"note.title\"  placeholder=\"Name\">\n      </mat-form-field>\n\n      <div class=\"label\">Content</div>\n      <app-ngx-editor [config]=\"editorConfig\" [(ngModel)]=\"htmlContent\"></app-ngx-editor>\n\n      <mat-form-field class=\"select-wrapper\">\n        <mat-select [(ngModel)]=\"note.selectedDatespan\" [placeholder]=\"'Expired in'\" disabled=\"disabled\">\n          <mat-option *ngFor=\"let datespan of datespans\" [value]=\"datespan.value\">\n            {{ datespan.name }}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n\n      <mat-form-field>\n        <input matInput type=\"text\" disabled=\"disabled\" [ngModel]=\"note.createDate | date:'MM/dd/yyyy HH:mm'\"  placeholder=\"Created date\">\n      </mat-form-field>\n    </div>\n\n    <button mat-button class=\"button\" routerLink='/edit/{{id}}'>\n      Edit\n    </button>\n    <button mat-button class=\"button\" (click)=\"Back()\">\n      Back\n    </button>\n  </mat-card>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/NoteView/note-view.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoteViewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_config_service__ = __webpack_require__("../../../../../src/app/Services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_notes_service__ = __webpack_require__("../../../../../src/app/Services/notes.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Contracts_INote__ = __webpack_require__("../../../../../src/app/Contracts/INote.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Services_datespans_service__ = __webpack_require__("../../../../../src/app/Services/datespans.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/toastr.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NoteViewComponent = (function () {
    function NoteViewComponent(config, notesService, location, route, datespansService, router, toastr) {
        var _this = this;
        this.config = config;
        this.notesService = notesService;
        this.location = location;
        this.route = route;
        this.datespansService = datespansService;
        this.router = router;
        this.toastr = toastr;
        this.id = this.route.snapshot.params.id;
        this.note = new __WEBPACK_IMPORTED_MODULE_5__Contracts_INote__["a" /* Note */]();
        // get datespans
        datespansService.GetDatespanList().subscribe(function (data) {
            _this.datespans = data;
        });
        // get data from server
        this.notesService.GetNote(this.id).subscribe(function (data) {
            if (!data) {
                _this.router.navigate(['/']);
            }
            _this.note = data;
            _this.htmlContent = _this.note.body;
        });
        // configure default config to view only
        this.editorConfig = this.config.EditorConfig;
        this.editorConfig.editable = false;
        this.editorConfig.toolbar = [[], [], [], []];
    }
    NoteViewComponent.prototype.Back = function () {
        return this.location.back();
    };
    NoteViewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/NoteView/note-view.component.html"),
            styles: [__webpack_require__("../../../../../src/app/NoteView/note-view.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__Services_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_4__Services_notes_service__["a" /* NotesService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_6__Services_datespans_service__["a" /* DatespansService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_7_ngx_toastr__["b" /* ToastrService */]])
    ], NoteViewComponent);
    return NoteViewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/Services/config.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};

var ConfigService = (function () {
    function ConfigService(editorCfg, environmentCfg) {
        this.editorCfg = editorCfg;
        this.environmentCfg = environmentCfg;
        this.editorConfig = editorCfg;
        this.environmentConfig = environmentCfg;
    }
    Object.defineProperty(ConfigService.prototype, "EditorConfig", {
        get: function () {
            return this.editorConfig;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConfigService.prototype, "EnvironmentConfig", {
        get: function () {
            return this.environmentConfig;
        },
        enumerable: true,
        configurable: true
    });
    ConfigService = __decorate([
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])('editorConfig')), __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Inject */])('environmentConfig')),
        __metadata("design:paramtypes", [Object, Object])
    ], ConfigService);
    return ConfigService;
}());



/***/ }),

/***/ "../../../../../src/app/Services/datespans.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatespansService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__("../../../../../src/app/Services/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DatespansService = (function () {
    function DatespansService(http, configService) {
        this.http = http;
        this.configService = configService;
        this.apiUrl = this.configService.EnvironmentConfig.apiBaseUrl;
    }
    DatespansService.prototype.GetDatespanList = function () {
        return this.http.get(this.apiUrl.concat('/datespans'));
    };
    DatespansService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */]])
    ], DatespansService);
    return DatespansService;
}());



/***/ }),

/***/ "../../../../../src/app/Services/notes.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__("../../../../../src/app/Services/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotesService = (function () {
    function NotesService(http, configService) {
        this.http = http;
        this.configService = configService;
        this.apiUrl = this.configService.EnvironmentConfig.apiBaseUrl;
    }
    NotesService.prototype.CreateNote = function (note) {
        return this.http.post(this.apiUrl.concat('/notes'), note);
    };
    NotesService.prototype.GetNoteList = function () {
        return this.http.get(this.apiUrl.concat('/notes'));
    };
    NotesService.prototype.GetNote = function (id) {
        return this.http.get(this.apiUrl.concat('/notes/').concat(id.toString()));
    };
    NotesService.prototype.UpdateNote = function (note) {
        return this.http.put(this.apiUrl.concat('/notes/').concat(note.id.toString()), note);
    };
    NotesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */]])
    ], NotesService);
    return NotesService;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_editor__ = __webpack_require__("../../../../ngx-editor/ngx-editor.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__NodeEdit_note_edit_component__ = __webpack_require__("../../../../../src/app/NodeEdit/note-edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__Services_config_service__ = __webpack_require__("../../../../../src/app/Services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__NoteList_note_list_component__ = __webpack_require__("../../../../../src/app/NoteList/note-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_flex_layout__ = __webpack_require__("../../../flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__NoteView_note_view_component__ = __webpack_require__("../../../../../src/app/NoteView/note-view.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__Services_notes_service__ = __webpack_require__("../../../../../src/app/Services/notes.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__Services_datespans_service__ = __webpack_require__("../../../../../src/app/Services/datespans.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ngx_toastr__ = __webpack_require__("../../../../ngx-toastr/toastr.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var appRoutes = [
    {
        path: 'edit/:id',
        component: __WEBPACK_IMPORTED_MODULE_6__NodeEdit_note_edit_component__["a" /* NoteEditComponent */]
    },
    {
        path: 'view/:id',
        component: __WEBPACK_IMPORTED_MODULE_13__NoteView_note_view_component__["a" /* NoteViewComponent */]
    },
    {
        path: 'new',
        component: __WEBPACK_IMPORTED_MODULE_6__NodeEdit_note_edit_component__["a" /* NoteEditComponent */]
    },
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_9__NoteList_note_list_component__["a" /* NoteListComponent */],
    },
    {
        path: '**',
        redirectTo: '/'
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__NodeEdit_note_edit_component__["a" /* NoteEditComponent */],
                __WEBPACK_IMPORTED_MODULE_9__NoteList_note_list_component__["a" /* NoteListComponent */],
                __WEBPACK_IMPORTED_MODULE_13__NoteView_note_view_component__["a" /* NoteViewComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_8__angular_router__["c" /* RouterModule */].forRoot(appRoutes),
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["c" /* MatFormFieldModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["g" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["d" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_5_ngx_editor__["a" /* NgxEditorModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["b" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["e" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["h" /* MatToolbarModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_material__["f" /* MatMenuModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_16_ngx_toastr__["a" /* ToastrModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__Services_config_service__["a" /* ConfigService */],
                [
                    {
                        provide: 'editorConfig', useFactory: function () {
                            return {
                                'editable': true,
                                'spellcheck': true,
                                'height': '5rem',
                                'minHeight': '2rem',
                                'placeholder': 'Enter text here...',
                                'translate': 'no',
                                'toolbar': []
                            };
                        }
                    },
                    {
                        provide: 'environmentConfig', useFactory: function () {
                            return {
                                'apiBaseUrl': 'http://localhost:3000/api'
                            };
                        }
                    }
                ],
                __WEBPACK_IMPORTED_MODULE_14__Services_notes_service__["a" /* NotesService */],
                __WEBPACK_IMPORTED_MODULE_15__Services_datespans_service__["a" /* DatespansService */]
            ],
            bootstrap: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_18" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map