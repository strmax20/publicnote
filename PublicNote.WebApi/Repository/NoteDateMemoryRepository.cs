﻿using System.Collections.Generic;
using System.Linq;
using PublicNote.Model.Datespans;
using PublicNote.Model.Datespans.Repository;

namespace PublicNote.WebApi.Repository
{
  public class NoteDateMemoryRepository : INoteDateSpanRepository
  {
    private List<NoteDateSpan> dateSpans = new NoteDateSpan[]
    {
        new NoteDateSpan { Name = "Never", Mask = "", Value = 1 },
        new NoteDateSpan { Name = "10 Minutes", Mask = "10m", Value = 2 },
        new NoteDateSpan { Name = "1 Hour", Mask = "1h", Value = 3 },
        new NoteDateSpan { Name = "1 Day", Mask = "1d", Value = 4 },
        new NoteDateSpan { Name = "1 Week", Mask = "1w", Value = 5 },
        new NoteDateSpan { Name = "1 Month", Mask = "1M", Value = 6 },
    }.ToList();

    public IQueryable<NoteDateSpan> DateSpans => this.dateSpans.AsQueryable();

    public NoteDateSpan NeverExpiredItem => dateSpans.First();
  }
}
