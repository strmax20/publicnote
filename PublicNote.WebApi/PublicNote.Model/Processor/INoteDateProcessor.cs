﻿using System;

namespace PublicNote.Model.Processor
{
  public interface INoteDateProcessor
  {
    DateTime Process(string format, DateTime beginTime);
  }
}