﻿namespace PublicNote.Model.Notes
{
  public interface INoteService
  {
    Note Add(Note note);
    Note Add(NoteDTO note);
    Note Get(int id);
    NoteDTO GetDTO(int id);
    Note[] GetList();
    NoteDTO[] GetDtoList();
    Note Update(Note note);
    Note Update(NoteDTO note);
  }
}
