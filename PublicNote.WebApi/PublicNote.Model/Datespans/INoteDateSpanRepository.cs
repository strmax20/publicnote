﻿using System.Linq;

namespace PublicNote.Model.Datespans.Repository
{
  public interface INoteDateSpanRepository
  {
    IQueryable<NoteDateSpan> DateSpans { get; }
    NoteDateSpan NeverExpiredItem { get; }
  }
}
