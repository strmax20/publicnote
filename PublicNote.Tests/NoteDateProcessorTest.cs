using System;
using Xunit;
using PublicNote.Model.Processor;

namespace PublicNote.Tests
{
  public class NoteDateProcessorTest
  {
    private NoteDateSpanMask[] defaultProcessorValues;

    public NoteDateProcessorTest()
    {
      defaultProcessorValues = (new NoteDateSpanMask[]
          {
                  new NoteDateSpanMask { Name = "minutes", Literal = "m" },
                  new NoteDateSpanMask { Name = "hours", Literal = "h" },
                  new NoteDateSpanMask { Name = "days", Literal = "d" },
                  new NoteDateSpanMask { Name = "weeks", Literal = "w" },
                  new NoteDateSpanMask { Name = "months", Literal = "M" },
          });
    }

    [Fact]
    public void Test1()
    {
      var date = DateTime.Now;
      var processor = new NoteDateProcessor(defaultProcessorValues);

      var calulated = processor.Process("1m 1h", date);
      var control = date.AddMinutes(1);
      control = control.AddHours(1);
      control = processor.Process("never", control);

      Assert.Equal(calulated, control);
    }
  }
}
