export interface INote
{
  id: number;
  title: string;
  body: string;
  neverExpired: boolean;
  createDate: Date;
  actualDate: Date;
  selectedDatespan: number;
}

export class Note implements INote
{
  public id: number;
  public title: string;
  public body: string;
  public createDate: Date;
  public actualDate: Date;
  public selectedDatespan: number;
  public neverExpired: boolean;

  constructor()
  {
    this.id = 0;
    this.title = '';
    this.body = '';
    this.createDate = new Date();
    this.actualDate = new Date();
    this.selectedDatespan = 0;
    this.neverExpired = true;
  }
}
