﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PublicNote.Model.Processor
{
  public class NoteDateProcessor : INoteDateProcessor
  {
    Dictionary<NoteDateSpanMask, int> timespans;

    public NoteDateProcessor(NoteDateSpanMask[] datespanMasks)
    {
      this.timespans = new Dictionary<NoteDateSpanMask, int>();

      datespanMasks.ToList().ForEach(datespan =>
          this.timespans.Add(datespan, 0));
    }

    public DateTime Process(string format, DateTime beginTime)
    {
      for(var index = 0; index < this.timespans.Count; index++)
      {
        var item = this.timespans.ElementAt(index);
        this.timespans[item.Key] = Parse(item.Key.Literal, format);
      }

      var date = beginTime.AddMinutes(GetTimespanValue("minutes"));
      date = date.AddHours(GetTimespanValue("hours"));
      date = date.AddDays(GetTimespanValue("days") + (GetTimespanValue("weeks") * 7));
      date = date.AddMonths(GetTimespanValue("months"));

      return date;
    }

    private int Parse(string literal, string pattern)
    {
      var rexp = new Regex($"\\d*[//{literal}]");

      var value = rexp.Match(pattern).Value
                .Split(literal.ToCharArray()).First();

      return (value.Equals(String.Empty)) ?
                  0 :
                  Convert.ToInt32(value);
    }

    private int GetTimespanValue(string label)
    {
      return this.timespans.FirstOrDefault(i => i.Key.Name.Equals(label)).Value;
    }
  }
}
