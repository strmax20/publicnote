﻿namespace PublicNote.Model.Datespans
{
  public class NoteDateSpan
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Mask { get; set; }
    public int Value { get; set; }
  }

  public static partial class Extensions
  {
    public static NoteDateSpanDTO ToDto(this NoteDateSpan self)
    {
      return new NoteDateSpanDTO
      {
        Name = self.Name,
        Value = self.Value
      };
    }
  }
}
