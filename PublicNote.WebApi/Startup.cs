﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PublicNote.Model.Notes;
using PublicNote.Model.Datespans;
using PublicNote.Model.Datespans.Repository;
using PublicNote.Model.Processor;
using PublicNote.WebApi.Repository;
using PublicNote.Model.Notes.Repository;

namespace PublicNote.WebApi
{
  public class Startup
  {
    public Startup(IHostingEnvironment env)
    {
      var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
          .AddEnvironmentVariables();
      Configuration = builder.Build();
    }

    public IConfigurationRoot Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      // Add framework services.
      services.AddMvc();

      // Allow CORS
      services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
      {
        builder.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader();
      }));

      services.AddScoped<ExceptionInfoFilter>();
      services.AddTransient<INoteService, NoteService>();
      services.AddTransient<INoteDateSpanService, NoteDateSpanService>();
      services.AddSingleton<INoteDateSpanRepository, NoteDateMemoryRepository>();

      // inject with factory default values
      services.AddSingleton<INoteDateProcessor, NoteDateProcessor>((ctx) => {
        return new NoteDateProcessor(new NoteDateSpanMask[]
              {
                  new NoteDateSpanMask { Name = "minutes", Literal = "m" },
                  new NoteDateSpanMask { Name = "hours", Literal = "h" },
                  new NoteDateSpanMask { Name = "days", Literal = "d" },
                  new NoteDateSpanMask { Name = "weeks", Literal = "w" },
                  new NoteDateSpanMask { Name = "months", Literal = "M" },
              });
      });
      // implements UnitOfWork by MVC opportunities
      services.AddSingleton<INotesRepository, NotesMemoryRepository>();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
    {
      loggerFactory.AddConsole(Configuration.GetSection("Logging"));
      loggerFactory.AddDebug();

      //app.UseStaticFiles();
      app.UseCors("CorsPolicy");
      app.UseMvc();
    }
  }
}
