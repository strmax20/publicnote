import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ConfigService} from "./config.service";
import {INote} from "../Contracts/INote";

@Injectable()
export class NotesService{

  private apiUrl: string;

  constructor(private http: HttpClient, private configService: ConfigService)
  {
    this.apiUrl = this.configService.EnvironmentConfig.apiBaseUrl;
  }

  public CreateNote(note: INote)
  {
    return this.http.post<INote>(
      this.apiUrl.concat('/notes'), note);
  }

  public GetNoteList()
  {
   return this.http.get(
     this.apiUrl.concat('/notes'));
  }

  public GetNote(id: number)
  {
    return this.http.get(
      this.apiUrl.concat('/notes/').concat(id.toString()));
  }

  public UpdateNote(note: INote)
  {
    return this.http.put<INote>(
      this.apiUrl.concat('/notes/').concat(note.id.toString()), note);
  }
}
