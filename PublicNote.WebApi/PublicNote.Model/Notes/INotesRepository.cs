﻿using System.Linq;

namespace PublicNote.Model.Notes.Repository
{
  public interface INotesRepository
  {
    IQueryable<Note> Notes { get; }
    Note Add(Note note);
    Note Update(Note note);
  }
}
