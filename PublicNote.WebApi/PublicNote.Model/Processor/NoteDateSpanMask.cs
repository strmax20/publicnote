﻿namespace PublicNote.Model.Processor
{
  public class NoteDateSpanMask
  {
    public string Name { get; set; }
    public string Literal { get; set; }
  }
}
