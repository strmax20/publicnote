﻿using System.Linq;
using PublicNote.Model.Datespans.Repository;

namespace PublicNote.Model.Datespans
{
  public class NoteDateSpanService : INoteDateSpanService
  {
    private INoteDateSpanRepository repository;

    public NoteDateSpanService(INoteDateSpanRepository repository)
    {
      this.repository = repository;
    }

    public NoteDateSpanDTO[] GetDtoList()
    {
      return this.repository.DateSpans.Select(item => item.ToDto()).ToArray();
    }

    public NoteDateSpan[] GetList()
    {
      return this.repository.DateSpans.ToArray();
    }
  }
}
