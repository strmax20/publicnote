interface IEditorConfig
{
  'editable': boolean;
  'spellcheck': boolean;
  'height': number | string;
  'minHeight':  number | string;
  'placeholder': string;
  'translate': string;
  'toolbar': string[][];
}
