﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using PublicNote.Model.Datespans;

namespace PublicNote.WebApi.Controllers
{
  [Route("api/[controller]")]
  [EnableCors("CorsPolicy")]
  public class DatespansController : Controller
  {
    private INoteDateSpanService datespanService;

    public DatespansController(INoteDateSpanService datespanService)
    {
      this.datespanService = datespanService;
    }

    // GET api/datespans
    [HttpGet]
    [ExceptionInfoFilter]
    public async Task<IEnumerable<NoteDateSpanDTO>> Get()
    {
      //IEnumerable<NoteDateSpanDTO> 
      return await Task.Run(() =>
      {
        return this.datespanService.GetDtoList();
      });
    }
  }
}
