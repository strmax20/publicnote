import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ConfigService} from "./config.service";

@Injectable()
export class DatespansService{

  private apiUrl: string;

  constructor(private http: HttpClient, private configService: ConfigService)
  {
    this.apiUrl = this.configService.EnvironmentConfig.apiBaseUrl;
  }

  public GetDatespanList()
  {
   return this.http.get(
     this.apiUrl.concat('/datespans'));
  }
}
