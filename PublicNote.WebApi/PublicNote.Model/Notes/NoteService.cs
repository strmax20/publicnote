﻿using System;
using System.Linq;
using PublicNote.Model.Datespans.Repository;
using PublicNote.Model.Notes.Repository;
using PublicNote.Model.Processor;

namespace PublicNote.Model.Notes
{
  public class NoteService : INoteService
  {
    private INotesRepository repository;
    private INoteDateProcessor dateProcessor;
    INoteDateSpanRepository datespans;

    public NoteService(INotesRepository repository, INoteDateSpanRepository datespans, INoteDateProcessor dateProcessor)
    {
      this.repository = repository;
      this.dateProcessor = dateProcessor;
      this.datespans = datespans;
    }

    public Note Add(Note note)
    {
      if (!this.repository.Notes.Count(item => item.Id.Equals(note.Id)).Equals(0))
        throw new ArgumentException();

      return this.repository.Add(note);
    }

    public Note Add(NoteDTO note)
    {
      var newNote = new Note
      {
        SelectedDatespan = note.SelectedDatespan,
        Body = note.Body,
        CreateDate = DateTime.Now,
        Id = 0,
        Title = note.Title
      };

      CalculateActuality(newNote);
      return this.repository.Add(newNote);
    }

    public NoteDTO GetDTO(int id)
    {
      return this.repository.Notes.FirstOrDefault(
            item => id.Equals(item.Id) && 
            (item.NeverExpired == true || item.ActualDate > DateTime.Now))?.ToDto();
    }

    public Note Get(int id)
    {
      return this.repository.Notes.FirstOrDefault(item => id.Equals(item.Id));
    }

    public NoteDTO[] GetDtoList()
    {
      var actualItems = this.repository.Notes.Where(item => 
        item.ActualDate >= DateTime.Now || 
        item.NeverExpired == true);

      return actualItems.Select(item => item.ToDto()).ToArray();
    }

    public Note[] GetList()
    {
      return this.repository.Notes.ToArray();
    }

    public Note Update(Note note)
    {
      var target = this.repository.Notes.FirstOrDefault(item => item.Id.Equals(note.Id));

      // if not found
      if (target.Equals(default(Note)))
      {
        throw new ArgumentException();
      }

      // get original create date
      note.CreateDate = target.CreateDate;
      CalculateActuality(note);
      return this.repository.Update(note);
    }

    public Note Update(NoteDTO note)
    {
      return this.Update(note.ToNote());
    }

    private void CalculateActuality(Note note)
    {
      note.ActualDate = this.dateProcessor.Process(
      this.datespans.DateSpans.First(item => item.Value.Equals(note.SelectedDatespan)).Mask, note.CreateDate);

      note.NeverExpired = this.datespans.NeverExpiredItem.Value.Equals(note.SelectedDatespan);
    }
  }
}
