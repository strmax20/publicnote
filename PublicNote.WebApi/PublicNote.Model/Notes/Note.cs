﻿using System;

namespace PublicNote.Model.Notes
{
  public class Note
  {
    public DateTime ActualDate { get; set; }
    public string Body { get; set; }
    public DateTime CreateDate { get; set; }
    public int Id { get; set; }
    public string Title { get; set; }
    public bool NeverExpired { get; set; }
    public int SelectedDatespan { get; set; }
  }

  public static partial class Extensions
  {
    public static void Update(this Note self, Note newNote)
    {
      self.ActualDate = newNote.ActualDate;
      self.Body = newNote.Body;
      self.CreateDate = newNote.CreateDate;
      self.Id = newNote.Id;
      self.NeverExpired = newNote.NeverExpired;
      self.Title = newNote.Title;
      self.SelectedDatespan = newNote.SelectedDatespan;
    }

    public static NoteDTO ToDto(this Note self)
    {
      return new NoteDTO
      {
        ActualDate = self.ActualDate,
        Body = self.Body,
        CreateDate = self.CreateDate,
        SelectedDatespan = self.SelectedDatespan,
        Title = self.Title,
        Id = self.Id,
        NeverExpired = self.NeverExpired
      };
    }
  }
}
