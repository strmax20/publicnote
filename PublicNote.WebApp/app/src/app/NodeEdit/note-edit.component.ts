import {Component} from '@angular/core';
import {Location} from '@angular/common';
import {ConfigService} from '../Services/config.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotesService} from '../Services/notes.service';
import {INote, Note} from '../Contracts/INote';
import {DatespansService} from '../Services/datespans.service';
import {IDatespan} from '../Contracts/IDatespan';
import {ToastrService} from 'ngx-toastr';

@Component({
  templateUrl: './note-edit.component.html',
  selector: 'app-root',
  styleUrls: ['./note-edit.component.css']
})

export class NoteEditComponent
{
  private editorConfig: IEditorConfig;
  private htmlContent: string;
  private note: INote;
  private datespans: IDatespan[];
  private readonly id: number;


  constructor(
    private config: ConfigService,
    private notesService: NotesService,
    private location: Location,
    private route: ActivatedRoute,
    private datespansService: DatespansService,
    private router: Router,
    private toastr: ToastrService)
  {
    this.id = this.route.snapshot.params.id;
    this.note = new Note();

    // get datespans
    datespansService.GetDatespanList().subscribe(data => {
      this.datespans = data as IDatespan[];
    });

    // get data from server
    if (this.id != null)
    {
      this.notesService.GetNote(this.id).subscribe(data => {
        if (!data)
        {
          this.router.navigate(['/']);
        }

        this.note = data as INote;
        this.htmlContent = this.note.body;
      });
    }

    // configure default config to edit mode
    this.editorConfig = this.config.EditorConfig;
    this.editorConfig.editable = true;

    // apply default toolbars set
    this.editorConfig.toolbar = [];
  }

  public Back()
  {
    return this.location.back();
  }

  public Save()
  {
    this.note.body = this.htmlContent;

    (this.id != null) ?
      this.notesService.UpdateNote(this.note).subscribe(info =>
      {
        console.log(info);
        this.toastr.success('Note '.concat(info.title).concat(' updated'), 'Success');
      }) :
      this.notesService.CreateNote(this.note).subscribe(info =>
      {
        console.log(info);
        this.toastr.success('Note '.concat(info.title).concat(' created'), 'Success');
      })
  }
}
