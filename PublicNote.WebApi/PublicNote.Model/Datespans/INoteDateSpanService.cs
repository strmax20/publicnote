﻿namespace PublicNote.Model.Datespans
{
  public interface INoteDateSpanService
  {
    NoteDateSpan[] GetList();
    NoteDateSpanDTO[] GetDtoList();
  }
}
