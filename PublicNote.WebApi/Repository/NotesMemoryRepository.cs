﻿using PublicNote.Model.Notes;
using PublicNote.Model.Notes.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace PublicNote.WebApi.Repository
{
  public class NotesMemoryRepository : INotesRepository
  {
    static Mutex mutex = new Mutex();
    List<Note> notes;

    private int lastId = 5;

    public IQueryable<Note> Notes => notes.AsQueryable();

    public NotesMemoryRepository()
    {
      // test data
      this.notes = new List<Note>();
      this.notes.Add(new Note { SelectedDatespan = 1, Body = "Body one", CreateDate = DateTime.Now, Id = 1, Title = "title 1", NeverExpired = true });
      this.notes.Add(new Note { SelectedDatespan = 1, Body = "Body two", CreateDate = DateTime.Now, Id = 2, Title = "title 2", NeverExpired = true });
      this.notes.Add(new Note { SelectedDatespan = 1, Body = "Body three", CreateDate = DateTime.Now, Id = 3, Title = "title 3", NeverExpired = true });
      this.notes.Add(new Note { SelectedDatespan = 1, Body = "Body four", CreateDate = DateTime.Now, Id = 4, Title = "title 4", NeverExpired = true });
      this.notes.Add(new Note { SelectedDatespan = 1, Body = "Body five", CreateDate = DateTime.Now, Id = 5, Title = "title 5", NeverExpired = true });
    }

    public Note Add(Note note)
    {
      mutex.WaitOne();
      if (note.Id == 0)
      {
        note.Id = this.GetNextId();
      }
      notes.Add(note);
      mutex.ReleaseMutex();

      return note;
    }

    public Note Update(Note note)
    {
      mutex.WaitOne();
      var target = this.notes.FirstOrDefault(item => item.Id.Equals(note.Id));
      target.Update(note);
      mutex.ReleaseMutex();

      return note;
    }

    private int GetNextId()
    {
      mutex.WaitOne();
      var id = ++this.lastId;
      mutex.ReleaseMutex();

      return id;
    }
  }
}
