import {Component} from '@angular/core';
import {Location} from '@angular/common';
import {ConfigService} from '../Services/config.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotesService} from '../Services/notes.service';
import {INote, Note} from '../Contracts/INote';
import {DatespansService} from '../Services/datespans.service';
import {IDatespan} from '../Contracts/IDatespan';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './note-view.component.html',
  styleUrls: ['./note-view.component.css']
})
export class NoteViewComponent{

  private editorConfig: IEditorConfig;
  private htmlContent: string;
  private note: INote;
  private datespans: IDatespan[];
  private readonly id: number;

  constructor(
    private config: ConfigService,
    private notesService: NotesService,
    private location: Location,
    private route: ActivatedRoute,
    private datespansService: DatespansService,
    private router: Router,
    private toastr: ToastrService)
  {
    this.id = this.route.snapshot.params.id;
    this.note = new Note();

    // get datespans
    datespansService.GetDatespanList().subscribe(data => {
      this.datespans = data as IDatespan[];
    });

    // get data from server
    this.notesService.GetNote(this.id).subscribe(data => {

      if (!data)
      {
        this.router.navigate(['/']);
      }
      this.note = data as INote;
      this.htmlContent = this.note.body;
    });

    // configure default config to view only
    this.editorConfig = this.config.EditorConfig;
    this.editorConfig.editable = false;
    this.editorConfig.toolbar = [[],[],[],[]];
  }

  public Back()
  {
    return this.location.back();
  }
}
