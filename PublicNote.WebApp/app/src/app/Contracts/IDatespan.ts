export interface IDatespan
{
  name: string;
  value: number;
}
